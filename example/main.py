"""Simulation from parameter file."""

from matplotlib import pyplot as plt

from acsim import Spacecraft
from acsim import PostProcessor

from acsim import plot_result
from acsim import plot_fom

# Define user parameter
param = 'user-params.json'

# Execute simulation
spacecraft  = Spacecraft.from_parameter_file(param)
result_path = spacecraft.solve(param)

# Post process
directory  = result_path.split('/')[0]
result     = result_path.split('/')[1]
pp = PostProcessor(directory, result=result)

# Visualize
plot_result(result_path)
plot_fom(directory, result=result)
plt.show()