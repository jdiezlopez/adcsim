import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib import font_manager
import warnings
import numpy as np

def plot_quaternion(*args, **kwargs):

    if len(args) > 2:
        raise TypeError('Too many input arguments: ' + str(len(args)))

    if len(args) == 1:
        q = args[0]
        t = np.arange(0, len(args[0]), 1)
        x_label = 'Sample'
    else:
        t = args[0]
        q = args[1]
        t_unit = kwargs.pop('t_unit', 'sec')
        if t_unit == 'min':
            t = t/60
        elif t_unit == 'h':
            t = t/3600
        x_label = 'Time $t$ [' + t_unit + ']'

    magnitude = kwargs.pop('magnitude', False)
    show_plot = kwargs.pop('show_plot', False)

    for key, val in kwargs.items():
        unused_argument = ("Unsued but specified argument: \'{0}\' = {1}"
                            .format(key,val))
        warnings.warn(unused_argument)

    ax = plt.gca()
    ax.plot(t, q[0:,0], 'k', linewidth=2, label='$q_s$')
    ax.plot(t, q[0:,1], 'r', linewidth=2, label='$q_x$')
    ax.plot(t, q[0:,2], 'g', linewidth=2, label='$q_y$')
    ax.plot(t, q[0:,3], 'b', linewidth=2, label='$q_z$')
    if magnitude:
        q_mag = np.linalg.norm(q, axis=1)
        ax.plot(t, q_mag, 'k--', linewidth=2, label=r'$\bar{q}$')
    ax.grid()
    ax.legend()
    ax.set_title('Attitude Quaternion')
    ax.set_xlabel(x_label)
    ax.set_ylabel('Quaternion $q$ [ ]')
    if show_plot:
        plt.draw()

    return ax