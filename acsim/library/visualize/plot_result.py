import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
from pyquaternion import Quaternion
import acsim

def plot_result(rname):
    res = acsim.Result(rname)

    sc_df = res.get_spacecraft_telemetry()
    ac_df = res.get_actuator_telemetry()
    co_df = res.get_controller_telemetry()

    qe = res.get_attitude_error()
    we = res.get_rate_error()
    phi_e = res.get_attitude_error_angle()

    fig_sc = plt.figure(rname)
    hq = plt.subplot(4, 1, 1)
    # true state
    plt.plot(sc_df['t'], sc_df['qs'], 'k', linewidth=2)
    plt.plot(sc_df['t'], sc_df['qx'], 'r', linewidth=2)
    plt.plot(sc_df['t'], sc_df['qy'], 'g', linewidth=2)
    plt.plot(sc_df['t'], sc_df['qz'], 'b', linewidth=2)
    # mpc predicted state
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['qs_k+1'][0:-1], 'ok', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['qx_k+1'][0:-1], 'or', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['qy_k+1'][0:-1], 'og', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['qz_k+1'][0:-1], 'ob', linewidth=2)
    # target state
    plt.step(co_df['t'], co_df['qs_c'], ':k', where='post',linewidth=2)
    plt.step(co_df['t'], co_df['qx_c'], ':r', where='post',linewidth=2)
    plt.step(co_df['t'], co_df['qy_c'], ':g', where='post',linewidth=2)
    plt.step(co_df['t'], co_df['qz_c'], ':b', where='post',linewidth=2)
    plt.ylabel('Quaternion []')
    plt.grid()

    hw = plt.subplot(4, 1, 2, sharex=hq)
    # true state
    plt.plot(sc_df['t'], np.rad2deg(sc_df['wx']), 'r', linewidth=2)
    plt.plot(sc_df['t'], np.rad2deg(sc_df['wy']), 'g', linewidth=2)
    plt.plot(sc_df['t'], np.rad2deg(sc_df['wz']), 'b', linewidth=2)
    # mpc predicted state
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], np.rad2deg(co_df['wx_k+1'][0:-1]), 'or', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], np.rad2deg(co_df['wy_k+1'][0:-1]), 'og', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], np.rad2deg(co_df['wz_k+1'][0:-1]), 'ob', linewidth=2)
    # target state
    plt.step(co_df['t'], np.rad2deg(co_df['wx_c']), ':r', where='post', linewidth=2)
    plt.step(co_df['t'], np.rad2deg(co_df['wy_c']), ':g', where='post', linewidth=2)
    plt.step(co_df['t'], np.rad2deg(co_df['wz_c']), ':b', where='post', linewidth=2)
    plt.ylabel('Angular rates [deg/s]')
    plt.grid()

    hh = plt.subplot(4, 1, 3, sharex=hq)
    # true state
    plt.plot(ac_df['t'], ac_df['hx'], 'r', linewidth=2)
    plt.plot(ac_df['t'], ac_df['hy'], 'g', linewidth=2)
    plt.plot(ac_df['t'], ac_df['hz'], 'b', linewidth=2)
    # mpc predicted state
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['hx_k+1'][0:-1], 'or', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['hy_k+1'][0:-1], 'og', linewidth=2)
    plt.plot(co_df['t'][0:-1]+co_df['t'][1]-co_df['t'][0], co_df['hz_k+1'][0:-1], 'ob', linewidth=2)
    plt.ylabel('Actuator Angular Momentum []')
    plt.xlabel('Time t [sec]')
    plt.grid()

    hu = plt.subplot(4, 1, 4, sharex=hq)
    plt.step(co_df['t'], co_df['ux'], 'r', where='post', linewidth=2)
    plt.step(co_df['t'], co_df['uy'], 'g', where='post', linewidth=2)
    plt.step(co_df['t'], co_df['uz'], 'b', where='post', linewidth=2)
    plt.ylim(-1.1, 1.1)
    plt.ylabel('Control Input []')
    plt.xlabel('Time t [sec]')
    plt.grid()

    fig_co = plt.figure('controller: '+rname)
    hqe = plt.subplot(4, 1, 1, sharex=hq)
    plt.step(co_df['t'], qe[0,:], 'k', where='post', linewidth=2)
    plt.step(co_df['t'], qe[1,:], 'r', where='post', linewidth=2)
    plt.step(co_df['t'], qe[2,:], 'g', where='post', linewidth=2)
    plt.step(co_df['t'], qe[3,:], 'b', where='post', linewidth=2)
    plt.ylabel('MPC $q_e$ []')
    plt.grid()

    hwe = plt.subplot(4, 1, 2, sharex=hq)
    plt.step(co_df['t'], we[0,:], 'r', where='post', linewidth=2)
    plt.step(co_df['t'], we[1,:], 'g', where='post', linewidth=2)
    plt.step(co_df['t'], we[2,:], 'b', where='post', linewidth=2)
    plt.ylabel('MPC $\omega_e$ [deg/s]')
    plt.grid()

    hwe = plt.subplot(4, 1, 3, sharex=hq)
    plt.plot(co_df['t'], co_df['t_cpu'], 'ok', linewidth=2)
    plt.ylabel('MPC $T_{cpu}$ [s]')
    plt.grid()

    phi_e_min = min(phi_e, key=abs)
    phi_e_max = max(phi_e, key=abs)
    hphie = plt.subplot(4, 1, 4, sharex=hq)
    plt.step(co_df['t'], phi_e, 'k', where='post', linewidth=2)
    plt.title(["Max = {}    Min = {}".format(phi_e_max, phi_e_min)])
    plt.ylabel('MPC $\phi_e$ [deg]')
    plt.xlabel('Time t [sec]')
    plt.grid()

    return fig_sc, fig_co
