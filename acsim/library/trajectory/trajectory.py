"""Trajectory Class.

This class provides a trajectory object for attitude control.
"""

import numpy as np
import copy as cp
import poliastro as pa
from astropy import units as u
from poliastro.bodies import Earth
from poliastro.twobody import Orbit as PaOrbit
from pyquaternion import Quaternion

from ..util import load_params


class Trajectory():
    """Trajectory object for commanded attitude."""

    def __init__(self,
                 t=10,
                 dt=0.5,
                 q=np.array([1, 0, 0, 0]),
                 w=np.array([0, 0, 0])):
        """Initialize trajectory object."""
        self.__t = np.arange(0, t, dt)
        self.__dt = dt
        self.set_trajectory(np.zeros((7, self.__t.shape[0])))
        self.set_target_attitude(q)
        self.set_target_rate(w)

    @classmethod
    def from_parameter_file(cls, fname):
        """Create trajectory from parameter file."""
        params = load_params(fname)
        trajectory_params = params['trajectory']

        trajectory = cls(t=params['simulation']['t_end'],
                         dt=params['simulation']['dt_solve'],
                         q=np.array(trajectory_params['attitude']),
                         w=np.array(trajectory_params['angular_rate']))

        return trajectory

    def set_trajectory(self, x):
        """Set the complete state trajectory."""
        self.__trajectory = x

    def set_target_attitude(self, q):
        """Set a target attitude for the complete trajectory."""
        if type(q) == Quaternion:
            self.__trajectory[0:4, :] = q.q.reshape(4,-1)
        else:
            self.__trajectory[0:4, :] = q.reshape(4,-1)

    def set_target_rate(self, w):
        """Set a target angular rates for the complete trajectory."""
        self.__trajectory[4:7, :] = w.reshape(3,-1)

    def get_trajectory(self):
        """Return the trajectory."""
        x = self.__trajectory
        return x

    def get_attitude_trajectory(self):
        """Return the attitude trajectory."""
        q = self.__trajectory[0:4, :]
        return q

    def get_rate_trajectory(self):
        """Return the angular rate trajectory."""
        w = self.__trajectory[4:7, :]
        return w

    def set_time(self, t):
        """Set the time array of the trajectory."""
        self.__t = t

    def get_time(self):
        """Return the time array of the trajectory."""
        t = self.__t
        return t

    def update_trajectory(self, horizon=1):
        """Update the trajectory for the current prediction horizon."""
        x = self.get_trajectory()

        if x.shape[1] <= horizon + 1:
            x = np.hstack((x, x[:, -1].reshape(7, 1)))

        x = x[:, 1:]
        self.set_trajectory(x)

        t = self.get_time()
        if t.shape[0] <= horizon + 1:
            t = np.hstack((t, t[-1]+self.__dt))
        t = t[1:]
        self.set_time(t)


class Orbit():
    def __init__(self,
                 altitude=600,
                 nu=0):
        self.__planet = Earth
        self.__ss = PaOrbit.circular(self.__planet,
                                     alt=altitude*u.km,
                                     arglat=nu*u.deg,)
        self.__r = self.__ss.r.to(u.m).value
        self.__v = self.__ss.v.to(u.m / u.s).value

    def get_pa_orbit(self):
        return self.__ss

    def propagate(self, t):
        self.__ss = self.__ss.propagate(t * u.s)
        self.__r = self.__ss.r.to(u.m).value
        self.__v = self.__ss.v.to(u.m / u.s).value

    def get_position(self):
        return np.array(self.__r)

    def get_velocity(self):
        return np.array(self.__v)


class NadirPointing(Trajectory):
    "Trajectory class for nadir pointing."

    def __init__(self, t, dt, orbit=Orbit()):
        t += 30*dt
        self.__t = np.arange(0, t-dt, dt)
        self.__dt = dt
        self.__orbit = orbit
        self.__r = self.__orbit.get_position()
        self.__v = self.__orbit.get_velocity()
        self.__initialize_trajectory()
        self.__update_nadir_trajectory()
        super().__init__(t=t, dt=dt, q=self.__q, w=self.__w)

    @classmethod
    def from_parameter_file(cls, fname):
        """Create trajectory from parameter file."""
        params = load_params(fname)

        orbit = Orbit(altitude=params['trajectory']['orbit']['altitude'],
                      nu=params['trajectory']['orbit']['nu'])

        trajectory = cls(t=params['simulation']['t_end'],
                         dt=params['simulation']['dt_solve'],
                         orbit=orbit)
        return trajectory

    def __initialize_trajectory(self):
        init_orbit = self.__orbit.get_pa_orbit()
        init_orbit = init_orbit.propagate_to_anomaly(0*u.deg)
        r = init_orbit.r.to(u.m).value
        v = init_orbit.v.to(u.m / u.s).value

        self.__qil0 = self.__calculate_lhlv_frame(r, v)
        qil_init = self.__calculate_lhlv_frame(self.__r, self.__v)

        q = (Quaternion(qil_init).inverse * Quaternion(self.__qil0))
        q = (Quaternion(self.__qil0).inverse * Quaternion(qil_init))

        w = self.__calculate_rate(r, v)
        # e = np.abs(q.axis)
        e = np.array([0, 1, 0])

        self.__q = q.q.reshape(4,1)
        self.__w = -(w * e).reshape(3,1)

    def __calculate_rate(self, r, v):
        w = np.abs(np.linalg.norm(v) / np.linalg.norm(r))
        return w

    def __calculate_lhlv_frame(self, r, v):
        "Calculate the transformation from LVLH to inertial frame qil"
        T = np.zeros((3, 3))
        T[:, 2] = - r / np.linalg.norm(r)
        T[:, 1] = - np.cross(r, v) / np.linalg.norm(np.cross(r, v))
        T[:, 0] = np.cross(T[:, 1], T[:, 2]) / np.linalg.norm(np.cross(T[:, 1], T[:, 2]))

        q = Quaternion(matrix=T)

        return q.q.reshape(4, 1)

    def __update_nadir_trajectory(self):
        for _ in self.__t:
            self.__orbit.propagate(self.__dt)
            r = self.__orbit.get_position()
            v = self.__orbit.get_velocity()
            self.__r = np.vstack((self.__r, r))
            self.__v = np.vstack((self.__v, v))

            qil = self.__calculate_lhlv_frame(r, v)
            # q = (Quaternion(qil).inverse * Quaternion(self.__qil0))
            q = (Quaternion(self.__qil0).inverse * Quaternion(qil))
            e = np.abs(q.axis)

            w = self.__calculate_rate(r, v)

            self.__q = np.hstack((self.__q, q.q.reshape(4,1)))
            self.__w = np.hstack((self.__w, -(w * e).reshape(3,1)))

class SwathWidening(NadirPointing):
    def __init__(self, t, dt, dt_pic, dw_pic, offset_axis=[1, 0, 0], orbit=Orbit()):
        super().__init__(t=t, dt=dt, orbit=orbit)
        self.__q_np = cp.deepcopy(self.get_attitude_trajectory())
        self.__w_np = cp.deepcopy(self.get_rate_trajectory())
        self.__dt = dt
        self.set_maneuver_timing(dt_pic)
        self.set_maneuver_offset(dw_pic, offset_axis)
        self._update_attitude_trajectory()

    @classmethod
    def from_parameter_file(cls, fname):
        """Create trajectory from parameter file."""
        params = load_params(fname)

        orbit = Orbit(altitude=params['trajectory']['orbit']['altitude'],
                      nu=params['trajectory']['orbit']['nu'])

        trajectory = cls(t=params['simulation']['t_end'],
                         dt=params['simulation']['dt_solve'],
                         dt_pic=params['trajectory']['swathwidening']['dt_pic'],
                         dw_pic=params['trajectory']['swathwidening']['dw_pic'],
                         offset_axis=params['trajectory']['swathwidening']['offset_axis'],
                         orbit=orbit)

        return trajectory

    def set_maneuver_timing(self, dt):
        """Set the time delta between maneuver points."""
        self.__dt_pic = dt

    def set_maneuver_offset(self, dw, axis):
        """Set the offset angle for the maneuver."""
        self.__dw_pic = dw
        self.__q_offset = Quaternion(angle=np.deg2rad(self.__dw_pic),
                                     axis=axis)

    def _check_maneuver_timing(self):
        """Check if the time at index 0 is a multiple of the maneuver timing."""
        try:
            t = self.get_time()[self.get_time().nonzero()[0][0]:]
            index = np.atleast_2d(np.where(t % self.__dt_pic == 0)[0])[0][0]
        except IndexError:
            index = 1

        return index

    def _update_attitude_offset(self):
        """Invert the offset quaternion for the maneuver."""
        self.__q_offset = self.__q_offset.inverse

    def _update_attitude_trajectory(self):
        """Calculate attitude trajectory."""
        q = np.array([1, 0, 0, 0]).reshape(4, 1)
        w = np.array([0, 0, 0]).reshape(3, 1)
        for quat in self.__q_np.transpose():
            temp = (Quaternion(quat) * self.__q_offset).q.reshape(4, 1)
            q = np.hstack((q, temp))
        for rate in self.__w_np.transpose():
            temp = self.__q_offset.inverse.rotate(rate).reshape(3, 1)
            w = np.hstack((w, temp))

        self.set_target_attitude(q[:, 1:])
        self.set_target_rate(w[:, 1:])

    def update_trajectory(self, horizon=1):
        """Overwrite: Update the trajectory for the current prediction horizon."""
        super().update_trajectory(horizon=horizon)

        if self.__q_np.shape[1] <= horizon + 1:
            self.__q_np = np.hstack((self.__q_np,
                                     self.__q_np[:, -1].reshape(4, 1)))
            self.__w_np = np.hstack((self.__w_np,
                                     self.__w_np[:, -1].reshape(3, 1)))

        self.__q_np = self.__q_np[:, 1:]
        self.__w_np = self.__w_np[:, 1:]

        index = self._check_maneuver_timing()

        if index == 0:
            self._update_attitude_offset()
            self._update_attitude_trajectory()


class StereoImaging(SwathWidening):
    def __init__(self, t, dt, dt_pic, dw_pic, offset_axis=[0, 1, 0], orbit=Orbit()):
        self.__dt_pic = dt_pic
        self.__dw_pic = dw_pic
        self.__offset_axis = offset_axis

        #super().__init__(t=t, dt=dt, dt_pic=self.__dt_pic, dw_pic=self.__dw_pic, orbit=orbit)
        super().__init__(t=t, dt=dt, dt_pic=dt_pic, dw_pic=dw_pic, offset_axis=offset_axis, orbit=orbit)

    @classmethod
    def from_parameter_file(cls, fname):
        """Create trajectory from parameter file."""
        params = load_params(fname)

        orbit = Orbit(altitude=params['trajectory']['orbit']['altitude'],
                      nu=params['trajectory']['orbit']['nu'])

        trajectory = cls(t=params['simulation']['t_end'],
                         dt=params['simulation']['dt_solve'],
                         dt_pic=params['trajectory']['stereoimaging']['dt_pic'],
                         dw_pic=params['trajectory']['stereoimaging']['dw_pic'],
                         offset_axis=params['trajectory']['stereoimaging']['offset_axis'],
                         orbit=orbit)

        return trajectory

#    def _update_attitude_offset(self):
#        """Overwrite: Update maneuver offset."""
#        try:
#            self.__dw_pic = self.__dw_pic[1:]
#            dw = self.__dw_pic[0]
#        except IndexError:
#            dw = 0
#        super().set_maneuver_offset(dw, self.__offset_axis)
#
#    def _update_maneuver_timing(self):
#        """Set the new time delta until next maneuver update"""
#        try:
#            self.__dt_pic = self.__dt_pic[1:]
#            dt = self.__dt_pic[0]
#        except IndexError:
#            dt = np.inf
#        super().set_maneuver_timing(dt)
#
#    def update_trajectory(self, horizon=2):
#        """Overwrite: Update the trajectory for the current prediction horizon."""
#        super().update_trajectory(horizon=horizon)
#
#        # update next maneuver dt if maneuver update took place
#        index = super()._check_maneuver_timing()
#        if index == 0:
#            self._update_maneuver_timing()
