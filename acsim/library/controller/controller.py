"""Controller Class.

This class provides a controller class for attitude control.
"""

import numpy as np
import casadi as cas
from pyquaternion import Quaternion
from ..trajectory import Trajectory
from ..trajectory import NadirPointing
from ..trajectory import SwathWidening
from ..trajectory import StereoImaging
from ..util import load_params


class Controller():
    """Controller class to inherit different attitude control objects."""

    def __init__(self):
        """Initialize controller object."""
        self.__u = np.array([0, 0, 0])
        self.__x = np.array([1, 0, 0, 0])
        self.__xc = np.array([1, 0, 0, 0])
        self.__logger = None

    def set_state(self, x):
        """Set the controlled systems current state."""
        self.__x = x

    def get_state(self):
        """Get the controlled systems current state."""
        return self.__x

    def set_target_state(self, x):
        """Set the current target system state."""
        self.__xc = x

    def set_target_attitude(self, q):
        """Set the current target attitude."""
        if type(q) == Quaternion:
            self.__xc[0:4] = q.q
        else:
            self.__xc[0:4] = q

    def get_target_state(self):
        """Set the current target system state."""
        return self.__xc

    def _update_sys_input(self, sys_input):
        """Update the control output."""
        self.__u = sys_input

    def get_u(self):
        """Return the current control output."""
        return self.__u

    def set_logger(self, logger):
        "Set a logger."
        self.__logger = logger

    def get_logger(self):
        "Return the logger."
        return self.__logger

class ModelPredictiveController(Controller):
    """Modelpredictive controller class.

    The modelpredictive controller class is designed for 3D attitude control
    using pico fluid-dynamic actuators.
    """

    def __init__(self, trajectory=Trajectory(),
                 control_cycle=0.5,
                 horizon=2,
                 states=10,
                 system_model_parameter={},
                 Qq=[0, 0, 0, 0],
                 Qw=[0, 0, 0],
                 Qa=[0, 0, 0],
                 R=[0, 0, 0],
                 Qqf=[0, 0, 0, 0],
                 Qwf=[0, 0, 0],
                 Qaf=[0, 0, 0],
                 Rf=[0, 0, 0]):
        """Initialize a model predictive controller object."""
        super().__init__()
        self.__trajectory = trajectory
        self.__states = states
        self.__hp = horizon
        self.__dt = control_cycle
        self.__Qq = cas.diag(Qq)
        self.__Qw = cas.diag(Qw)
        self.__Qa = cas.diag(Qa)
        self.__R = cas.diag(R)
        self.__Qqf = cas.diag(Qqf)
        self.__Qwf = cas.diag(Qwf)
        self.__Qaf = cas.diag(Qaf)
        self.__Rf = cas.diag(Rf)
        self.__sym_u_pfda = cas.MX.sym('u_pfda', 3)
        self.__sym_x = cas.MX.sym('x', self.__states)
        self.__system_model_params = system_model_parameter
        self.__init_sys_model_params()
        self.set_state(np.hstack((1, np.zeros(self.__states-1))))
        self.__init_pfda_rhs()
        self.__init_sat_dynamics()
        self.__init_sat_kinematics()
        self.__init_sat_system_model()
        self.__init_symbolic_qtimes()
        self.__init_opti()

    @classmethod
    def from_parameter_file(cls, fname):
        """Create model predictive controller from parameter file."""
        params = load_params(fname)
        mpc_params = params['mpc']

        if params['trajectory']['mode'] == 'manual':
            trajectory = Trajectory.from_parameter_file(fname)
        elif params['trajectory']['mode'] == 'nadirpointing':
            trajectory = NadirPointing.from_parameter_file(fname)
        elif params['trajectory']['mode'] == 'swathwidening':
            trajectory = SwathWidening.from_parameter_file(fname)
        elif params['trajectory']['mode'] == 'stereoimaging':
            trajectory = StereoImaging.from_parameter_file(fname)

        if mpc_params['true_system_model_parameter'] is True:
            system_model_parameter = {}
            system_model_parameter['K'] = params['pfda']['K']
            system_model_parameter['I'] = np.array(params['spacecraft']
                                                         ['inertia'])
        else:
            system_model_parameter = {}
            system_model_parameter['K'] = mpc_params['pfda_K']
            system_model_parameter['I'] = mpc_params['sat_I']

        mpc = cls(trajectory=trajectory,
                  control_cycle=mpc_params['control_cycle'],
                  horizon=mpc_params['horizon'],
                  states=mpc_params['states'],
                  system_model_parameter=system_model_parameter,
                  Qq=mpc_params['Qq'],
                  Qw=mpc_params['Qw'],
                  Qa=mpc_params['Qa'],
                  R=mpc_params['R'],
                  Qqf=mpc_params['Qqf'],
                  Qwf=mpc_params['Qwf'],
                  Qaf=mpc_params['Qaf'],
                  Rf=mpc_params['Rf'])

        return mpc

    def __init_sys_model_params(self):
        """Initialize system model parameters for spacecraft and pfdas."""
        self.__I = self.__system_model_params['I']

        self.__K = self.__system_model_params['K']

        u = cas.SX.sym('u')
        # c100 w150 v24 false
        # p = [0.93, 0.128, 0.428, 0.24]
        
        # c100 w150 v24
        # p = [0.93, 0.128, 4.278, 0.24]
        
        # r80 w150 v24
        p = [0.317, 0.039, 2.05, 0.102]

        T = (p[0]*u**2 + p[1]) / (u**4 + p[2]*u**2 + p[3])

        self.__T = cas.Function('T', [u], [T], ['u'], ['T'])

    def __init_symbolic_qtimes(self):
        """Initialize the quaternion product for casADi."""
        q1 = cas.MX.sym('q1', 4)
        q2 = cas.MX.sym('q2', 4)

        quat_multiply = cas.vertcat(q1[0] * q2[0] - cas.dot(q1[1:4], q2[1:4]),
                                    q1[0] * q2[1:4] + q2[0] * q1[1:4]
                                    + cas.cross(q1[1:4], q2[1:4]))

        self.__qtimes = cas.Function('quat_multiply', [q1, q2],
                                     [quat_multiply], ['q1', 'q2'], ['q'])

    def __init_pfda_rhs(self):
        "Initialize casADi rhs for the pfda assembly."
        x = self.__sym_x
        u_pfda = self.__sym_u_pfda
        K = self.__K
        T = self.__T

        rhs_pfda = cas.vertcat(-1 / T(u_pfda[0]) * x[7] + K / T(u_pfda[0]) * u_pfda[0],
                               -1 / T(u_pfda[1]) * x[8] + K / T(u_pfda[1]) * u_pfda[1],
                               -1 / T(u_pfda[2]) * x[9] + K / T(u_pfda[2]) * u_pfda[2])

        self.__pfda_rhs = rhs_pfda

    def __init_sat_kinematics(self):
        "Initialize casADi rhs for quaternion kinematics."
        x = self.__sym_x
        kin = cas.vertcat(-0.5 * cas.dot(x[1:4], x[4:7]),
                          0.5 * (x[0] * x[4:7] + cas.cross(x[1:4], x[4:7])))
        self.__sat_kinematics = kin

    def __init_sat_dynamics(self):
        "Initialize casADi rhs for satellite dynamics with pfdas."
        x = self.__sym_x
        u_pfda = self.__sym_u_pfda
        K = self.__K
        I_sat = cas.DM(self.__I)
        T = self.__T

        temp = cas.vertcat(-1 / T(u_pfda[0]) * x[7] + K / T(u_pfda[0]) * u_pfda[0],
                           -1 / T(u_pfda[1]) * x[8] + K / T(u_pfda[1]) * u_pfda[1],
                           -1 / T(u_pfda[2]) * x[9] + K / T(u_pfda[2]) * u_pfda[2])
        dyn = cas.inv(I_sat) @ (temp - cas.cross(x[4:7], (I_sat @ x[4:7] + x[7:10])))

        self.__sat_dynamics = dyn

    def __init_sat_system_model(self):
        """Initialize the satellite system model for casADi."""
        x = self.__sym_x
        u_pfda = self.__sym_u_pfda
        dt = self.__dt

        rhs = cas.vertcat(self.__sat_kinematics,
                          self.__sat_dynamics,
                          self.__pfda_rhs)

        f = cas.Function('f', [x, u_pfda], [rhs])
        ode = {}
        ode['x'] = x
        ode['ode'] = f(x, u_pfda)
        ode['p'] = u_pfda

        ode_opts = {}
        ode_opts['tf'] = dt
        # ode_opts['number_of_finite_elements'] = 2
        ode_opts['number_of_finite_elements'] = 5

        intg = cas.integrator('intg', 'rk', ode, ode_opts)

        res = intg(x0=x, p=u_pfda)
        x_next = res['xf']

        self.__F_sat = cas.Function('F', [x, u_pfda], [x_next],
                                    ['x', 'u_pfda'], ['x_next'])

    def __init_opti(self):
        """Initialize casADi's solver object."""
        self.__opti = cas.Opti()
        opts = {}
        # opts['qpsol'] = 'osqp'
        # opts['max_iter'] = 1000
        # opts['print_iteration'] = False
        # opts['print_header'] = False
        # opts['print_status'] = False

        # opts['qpsol_options'] = {}
        # opts['qpsol_options']['error_on_fail'] = False

        # self.__opti.solver('sqpmethod', opts)

        opts['print_time'] = False
        opts['ipopt'] = {'print_level': 0}
        self.__opti.solver('ipopt', opts)  # ('sqpmethod', {'qpsol':'osqp'})

        self.__opti_sol = None

        n = self.__hp
        self.__opti_x = self.__opti.variable(self.__states, n+1)
        self.__opti_q_e = self.__opti.variable(4, n+1)
        self.__opti_w_e = self.__opti.variable(3, n+1)
        self.__opti_u = self.__opti.variable(3, n)
        self.__opti_p = self.__opti.parameter(self.__states, 1)

        x = self.__opti_x
        u = self.__opti_u
        p = self.__opti_p

        F_sat = self.__F_sat

        self.__opti.subject_to(1 >= u[:])
        self.__opti.subject_to(-1 <= u[:])
        # self.__opti.subject_to(0 == u[:, -1])

        self.__opti.subject_to(x[:, 0] == p)

        for k in range(n):
            self.__opti.subject_to(x[:, k+1] == F_sat(x[:, k], u[:, k]))

    def update_control(self):
        """Solve the optimization problem on the current prediction horizon."""
        n = self.__hp
        x0 = self.get_state()
        trajectory = self.__trajectory.get_trajectory()
        opti = self.__opti
        x = self.__opti_x
        u = self.__opti_u

        p = self.__opti_p
        q_e = self.__opti_q_e
        w_e = self.__opti_w_e

        for k in range(n):
            q_c_inv = Quaternion(trajectory[0:4, k+1]).inverse.q
            q_e[:, k] = self.__qtimes(x[0:4, k+1], q_c_inv)
            q_e[:, k] = cas.sign(q_e[0, k]) * q_e[:, k]
            w_e[:, k] = (x[4:7, k+1] - trajectory[4:7, k+1])

        scalar_sub = np.zeros(np.shape(q_e))
        scalar_sub[0, :] = 1

        opti.minimize(cas.dot(q_e - scalar_sub,                 cas.transpose(cas.transpose(q_e - scalar_sub)               @ self.__Qq))
                      + cas.dot(w_e,                            cas.transpose(cas.transpose(w_e)                            @ self.__Qw))
                      + cas.dot(x[7:10, :],                     cas.transpose(cas.transpose(x[7:10, :])                     @ self.__Qa))
                      + cas.dot(u,                              cas.transpose(cas.transpose(u)                              @ self.__R))
                      + cas.dot(q_e[:, -1] - scalar_sub[:, -1], cas.transpose(cas.transpose(q_e[:, -1] - scalar_sub[:, -1]) @ self.__Qqf))
                      + cas.dot(w_e[:, -1],                     cas.transpose(cas.transpose(w_e[:, -1])                     @ self.__Qwf))
                      + cas.dot(x[7:10, -1],                    cas.transpose(cas.transpose(x[7:10, -1])                    @ self.__Qaf))
                      + cas.dot(u[:, -1],                       cas.transpose(cas.transpose(u[:, -1])                       @ self.__Rf)))

        opti.set_value(p, x0)

        if self.__opti_sol is not None:
            opti.set_initial(self.__opti_sol.value_variables())
            sol = opti.solve()
            self.__opti_sol = sol
        else:
            sol = opti.solve()
            self.__opti_sol = sol

        u = np.array(sol.value(u))
        super()._update_sys_input(u.reshape(3,-1)[:, 0])

        if self.get_logger() is not None:
            self.log()
        self.__trajectory.update_trajectory(horizon=self.__hp)

    def log(self):
        """Log the controller data to a file."""
        sol = self.__opti_sol
        t = self.__trajectory.get_time()[0]
        u = np.array(sol.value(self.__opti_u)).reshape(3,-1)[:,0]         # control value
        i = sol.stats()['iter_count']                       # iteration

        ## IPOPT Method
        j = np.array(sol.stats()['iterations']['obj'][-1])  # cost function
        t_cpu = (sol.stats()['t_proc_nlp_f']
                + sol.stats()['t_proc_nlp_g']
                + sol.stats()['t_proc_nlp_grad']
                + sol.stats()['t_proc_nlp_grad_f']
                + sol.stats()['t_proc_nlp_hess_l']
                + sol.stats()['t_proc_nlp_jac_g'])

        ## SQP Method
        # j = 0
        # t_cpu = (sol.stats()['t_proc_total'])

        x = np.array(sol.value(self.__opti_x)[:,1])         # predicted state
        x_c = self.__trajectory.get_trajectory()[:,0]
        q_e = np.array(sol.value(self.__opti_q_e)[:,0])     # attitude error
        w_e = np.array(sol.value(self.__opti_w_e)[:,0])     # rate error

        logger = self.get_logger()
        logger.log_mpc(t, u, i, t_cpu, j, x, x_c, q_e, w_e)
