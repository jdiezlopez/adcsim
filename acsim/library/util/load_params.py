"""Utility functions for the simulation."""

import json
import importlib.resources as importlib_resources

def load_params(file, develop=False):
    """Create parameter dictionary.

    Create parameter dict from 'default_params.json' and user params
    file. The default function only loads parameters from the user
    file which are declared in the default_params.json file.
    """

    dpname = 'default_params.json'
    # read default parameter file
    with importlib_resources.path('acsim.library', dpname) as p:
        with open(p, 'r') as f:
            params = json.load(f)

    # read user parameter file
    with open(file, 'r') as f:
        user_params = json.load(f)

    # override default parameters with user parameters
    if develop is True:
        params.update(user_params)
    else:
        # allow only parameter defined in 'default_params.json'
        for k in params.keys() & user_params.keys():
            for j in params[k].keys() & user_params[k].keys():
                if type(params[k][j]) is not dict:
                    params[k].update([(j, user_params[k][j])])
                else:
                    for l in params[k][j].keys() & user_params[k][j].keys():
                        params[k][j].update([(l, user_params[k][j][l])])

    # return params for simulation
    return params
