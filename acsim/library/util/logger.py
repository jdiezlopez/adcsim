"""Utility functions for the simulation."""

import json
import numpy as np
import os
from datetime import datetime
from .load_params import load_params

class Logger():
    """Logger class.

    Logs data such as results or meta data to files.
    """

    def __init__(self,
                 log_type="spacecraft",
                 dirname="results",
                 filename="result"):
        """Initialize logger object."""
        self.__dname = dirname
        self.__fname = filename
        self.__log_type = log_type

    def init_simulation_result(self, fname):
        "Create a result directory for the current simulation."
        params = load_params(fname)
        self.__dname = params['simulation']['result_dir']
        self.__fname = params['simulation']['result_name']

        # check if results directory exists
        if not os.path.isdir(self.__dname):
            os.mkdir(self.__dname)

        # create the current time stamp
        self.__timestamp = datetime.now().replace(microsecond=0).isoformat().replace(":","-")

        # create result folder for current simulation
        self.__result_dir = (self.__dname + "/" + self.__timestamp + "-"
                             + self.__fname)
        os.mkdir(self.__result_dir)

        # create parameters from default and user params and create a copy
        with open(self.__result_dir + "/" + "parameter.json", "w") as f:
            json.dump(params, f, indent=4)

        # create log files
        with open(self.__result_dir + "/" + "spacecraft.csv", "w") as f:
            header = ("t,"
                    + "qs,qx,qy,qz,"
                    + "wx,wy,wz\n")
            f.write(header)

        if params['spacecraft']['controller'] == 'mpc':
            with open(self.__result_dir + "/" + "controller.csv", "w") as f:
                header = ("t,"
                        + "ux,uy,uz,"
                        + "i,t_cpu,j,"
                        + "qs_k+1,qx_k+1,qy_k+1,qz_k+1,"
                        + "wx_k+1,wy_k+1,wz_k+1,"
                        + "hx_k+1,hy_k+1,hz_k+1,"
                        + "qs_c,qx_c,qy_c,qz_c,"
                        + "wx_c,wy_c,wz_c,"
                        + "qs_e,qx_e,qy_e,qz_e,"
                        + "wx_e,wy_e,wz_e\n")
                f.write(header)

        elif params['spacecraft']['controller'] == 'pdc':
            with open(self.__result_dir + "/" + "controller.csv", "w") as f:
                header = ("t,"
                        + "ux,uy,uz,"
                        + "qs_e,qx_e,qy_e,qz_e,"
                        + "wx_e,wy_e,wz_e\n")
                f.write(header)

        if params['spacecraft']['actuators'] is not None:
            with open(self.__result_dir + "/" + "actuator.csv", "w") as f:
                header = ("t,"
                        + "hx,hy,hz,"
                        + "taux,tauy,tauz\n")
                f.write(header)

    def log_spacecraft(self, time, q, w):
        data = np.c_[time, q, w]
        logfile = "spacecraft.csv"
        self.log(logfile, data)

    def log_actuators(self, time, h, tau):
        data = np.c_[time, h, tau]
        logfile = "actuator.csv"
        self.log(logfile, data)

    def log_controller(self, t, u, q_e, w_e):
        data = np.hstack((t, u, q_e, w_e)).reshape(1,-1)
        logfile = "controller.csv"
        self.log(logfile, data)

    def log_mpc(self, t, u, i, t_cpu, j, x_pred, x_c, q_e, w_e):
        data = np.hstack((t, u, i, t_cpu, j, x_pred, x_c, q_e, w_e)).reshape(1,-1)
        logfile = "controller.csv"
        self.log(logfile, data)

    def log(self, logfile, data):
        """Write data to the log file."""
        with open(self.__result_dir+"/"+logfile, 'a') as f:
            np.savetxt(f, data, delimiter=',', fmt='%1.16f')

    def get_result_dir(self):
        "Return the path to the log files."
        return self.__result_dir

    def get_solution(self):
        # TODO: return struct with loaded dataframes and dirname
        pass
