"""Utility functions for the simulation."""

import json
import os

def write_params(paramfile, params, fname=None):
    """Write parameter to a file.

    If paramfile is an existing parameter file, overwrite the parameters
    given in params.
    If paramfile does not exist, create the file and write params to
    it.
    If fname is given, write the parameters to the declared name.
    """

    # read user parameter file and overwrite parameters given in params
    if os.path.isfile(paramfile):
        with open(paramfile, 'r') as f:
            user_params = json.load(f)


        for k in params.keys():

            if k not in user_params.keys():
                user_params.update([(k, params[k])])
            else:
                for j in params[k].keys():

                    if j not in user_params[k].keys():
                        user_params[k].update([(j, params[k][j])])
                    else:

                        if type(params[k][j]) is not dict:
                            user_params[k].update([(j, params[k][j])])

                        else:
                            for l in params[k][j].keys():
                                user_params[k][j].update([(l, params[k][j][l])])

    else:
        user_params = params

    if fname is not None:
        paramfile = fname

    with open(paramfile, 'w') as f:
        json.dump(user_params, f, indent=4)

    return paramfile