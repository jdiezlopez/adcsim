"""Actuator Class.

This class provides an actuator class for attitude control,
"""

import numpy as np
from pyquaternion import Quaternion

from ..util import load_params
from .actuator import Actuator

class LinearPFDA(Actuator):
    """Linear pFDA class using a PT1 transferfunction for angular momentum."""

    def __init__(self, angular_momentum=0,
                 torque=0,
                 sys_input=0,
                 K=2,
                 T=0.5,
                 z=0):
        """Instanciate LinearPFDA object.

        Parameters
        alignment: Quaternion
            Orientation of the actuator
        angular_momentum: int
            Angular momentum of the actuator
        torque: int
            Torque of the actuator
        sys_input: int
            System input in range of -1 to 1
        K: int
            Gain of the PT1 transferfunction
        T: int
            Time constante of the PT1 transferfunction
        z: int
            Internal state to solve biproper representation of torque
        """
        self.__K = K
        self.__T = T
        self.__z = z
        super().__init__(angular_momentum=angular_momentum,
                         torque=torque,
                         sys_input=sys_input)

    @classmethod
    def from_parameter_file(cls, fname):
        """Create actuator from parameter file."""
        params = load_params(fname)
        pfda_params = params['pfda']
        pfda = cls(angular_momentum=pfda_params['angular_momentum'],
                   torque=pfda_params['torque'],
                   sys_input=pfda_params['sys_input'],
                   K=pfda_params['K'],
                   T=pfda_params['T'],
                   z=pfda_params['z'])

        return pfda

    def _rhs(self, t, x):
        """Actuator right-hand side function.

        Right-hand side function for actuator stand alone simulation, used for
        verification.
        """

        self.update_state(x)
        return self.get_state_derivatives()

    def update_state(self, x):
        """Update the actuator angular momentum and torque.

        Because of the biproper system behaviour of the pFDAs torque. Torque
        has to be calculated from the internal state z.

        Parameters
        H: int
            Angular momentum
        z: int
            Internal state to solve biproper representation of torque
        """

        H = x[0]
        z = x[1]

        self.__z = z
        u = self.get_input()

        tau = self.__z + self.__K / self.__T * u

        super().update_angular_momentum(H)
        super().update_torque(tau)

    def set_time_constant(self, T):
        """Set the time constant of the pFDA."""
        self.__T = T

    def set_gain(self, K):
        """Set the gain of the pFDA."""
        self.__K = K

    def get_gain(self):
        """Return the gain of the pFDA."""
        return self.__K

    def get_time_constant(self):
        """Return the time constant of the pFDA."""
        return self.__T

    def get_state(self):
        """Return the pFDA state containing angular momentum and z."""
        H = super().get_angular_momentum()
        z = self.__z
        return np.hstack((H, z))

    def get_state_derivative(self):
        """Return the derivatives of angular momentum and z."""
        H = super().get_angular_momentum()
        u = self.get_input()
        dh = -1/self.__T * H + self.__K/self.__T * u
        dz = -1/self.__T * self.__z - self.__K/(self.__T**2) * u

        # dh =               self.__z + self.__K/self.__T * u
        # dz = -1/self.__T * self.__z - self.__K/(self.__T**2) * u
        return np.hstack((dh, dz))
