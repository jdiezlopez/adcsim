"""Actuator Class.

This class provides an actuator class for attitude control,
"""

import numpy as np
from pyquaternion import Quaternion

from ..util import load_params
from .actuator import Actuator

class PFDA(Actuator):
    """Actuator class for angular momentum exchange based actuators."""
    """Linear pFDA class using a PT1 transferfunction for angular momentum."""

    def __init__(self, angular_momentum=0,
                 sys_input=0,
                 K=2):
        """Instanciate LinearPFDA object.

        Parameters
        alignment: Quaternion
            Orientation of the actuator
        angular_momentum: int
            Angular momentum of the actuator
        torque: int
            Torque of the actuator
        sys_input: int
            System input in range of -1 to 1
        K: int
            Gain of the PT1 transferfunction
        T: int
            Time constante of the PT1 transferfunction
        z: int
            Internal state to solve biproper representation of torque
        """
        self.set_gain(K)
        super().__init__(angular_momentum=angular_momentum,
                         torque=0,
                         sys_input=sys_input)
        self.set_input(sys_input)

    @classmethod
    def from_parameter_file(cls, fname):
        """Create actuator from parameter file."""
        params = load_params(fname)
        pfda_params = params['pfda']
        pfda = cls(angular_momentum=pfda_params['angular_momentum'],
                   sys_input=pfda_params['sys_input'],
                   K=pfda_params['K'])

        return pfda

    def __time_constant(self, u):
        """Return the time constant for the R80-W150-V24 pfda."""
        p = [0.317, 0.039, 2.05, 0.102]
        T =  (p[0]*u**2 + p[1]) / (u**4 + p[2]*u**2 + p[3])
        return T

    def __update_time_constant(self):
        u = self.get_input()
        T = self.__time_constant(u)
        self.__T = T

    def set_input(self, sys_input):
        """Set the system input of the actuator.

        Parameters
        sys_input: int
            System input in range of -1 to 1
        """
        super().set_input(sys_input)

        # update self.__T
        self.__update_time_constant()

        # update self.__tau
        H = super().get_angular_momentum()
        self.update_state(H)

    def _rhs(self, t, x):
        """Actuator right-hand side function.

        Right-hand side function for actuator stand alone simulation, used for
        verification.
        """
        h = x[0]

        self.update_state(h)
        return [self.get_state_derivative()]

    def update_state(self, h):
        """Update the actuator angular momentum and torque.

        Parameters
        h: int
            Angular momentum
        """

        u = self.get_input()
        T = self.get_time_constant()
        K = self.get_gain()

        tau = -1 / T * h + K / T * u

        super().update_angular_momentum(h)
        super().update_torque(tau)

    def set_gain(self, K):
        """Set the gain of the pFDA."""
        self.__K = K

    def get_gain(self):
        """Return the gain of the pFDA."""
        return self.__K

    def get_time_constant(self):
        """Return the time constant of the pFDA."""
        return self.__T

    def get_state(self):
        """Return angular momentum."""
        return super().get_angular_momentum()

    def get_state_derivative(self):
        """Return the derivative of angular momentum."""
        return super().get_torque()
